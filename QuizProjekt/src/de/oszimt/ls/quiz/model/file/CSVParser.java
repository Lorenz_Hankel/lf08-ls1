package de.oszimt.ls.quiz.model.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import de.oszimt.ls.quiz.model.Model;
import de.oszimt.ls.quiz.model.Schueler;

public class CSVParser {
	private File datei;

	/**
	 * Erstelle XML Datei
	 *
	 * @param pfad, Pfad zur Datei
	 */
	public CSVParser(String pfad) {
		this.datei = new File(pfad);
	}

	/**
	 * Lädt die XML Datei
	 * 
	 * @return Model der XML-Datei
	 */
	public Model laden() {
		Model model = new Model();

		// Datei laden

		// Variablen deklarieren und initialisieren

		try {
			// ich definiere das Buffered Reader Objekt mit einem neuen File Reader Objekt
			BufferedReader reader = new BufferedReader(new FileReader(datei));
			String zeile = "";
			String[] zeileArray = null;
			int i = 0;
			while (zeile != null) {
				zeile = reader.readLine();
				zeileArray = zeile.split(";");
				// Abfangen von der Überschriften- Zeile
				if (i == 0) {

				} else {
					model.getAlleSchueler()
							.add(new Schueler(zeileArray[0], zeileArray[1], Integer.parseInt(zeileArray[2]),
									Integer.parseInt(zeileArray[3]), Integer.parseInt(zeileArray[4])));
				}
				i++;
			} // Ende der While-Schleife

		} catch (FileNotFoundException e) {
			System.out.println("keine Datei gefunden mit dem Pfad: " + datei.getAbsolutePath() + ".");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Dateizugriff verwährt!");
			e.printStackTrace();
		}

		return model;
	}
}
