import java.io.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;

/* Arbeitsauftrag:  Erstellen Sie ein DOM-Dokument gem�� den Vorgaben 
 * 					aus der Datei "Vorgabe_f�r_Ausgabedatei.xml" 
 * 					und sichern Sie es als XML in eine Datei 
 * 					mit dem Filename "buchhandlung.xml".
 * 	
 *               
 */

/**
 * Diese Klasse ist die Main Klasse für das Schreiben der XML Dateien
 * 
 * @author Lorenz Hankel
 * @version 02.09.2021
 *
 */
public class WriteBookstoreDatei {

	/**
	 * Dies ist die Main-Methode der Klasse WriteBookstoreDatei.<br>
	 * Es soll eine XML Datei mit dem Namen "buchhandlung.xml" <br>
	 * geschrieben werden.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		try {

			// Document vorbereiten
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dbuilder = factory.newDocumentBuilder();
			Document doc = dbuilder.newDocument();

			Element buchhandlung = doc.createElement("buchhandlung");
			doc.appendChild(buchhandlung);

			Element buch = doc.createElement("buch");
			buch.setAttribute("lang", "de");

			buch.appendChild(doc.createTextNode("Einf�hrung in OOP"));

			buchhandlung.appendChild(buch);

			// XML Schreiben
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			// Ziel f�r das Schreiben setzen
			StreamResult result = new StreamResult(new File("buchhandlung.xml"));

			// Datei schreiben
			transformer.transform(source, result);

		} catch (ParserConfigurationException e) {
			System.err.println("Es gab einen Fehler mit einen Parser.\n" + e.getMessage());
		} catch (TransformerConfigurationException e) {
			System.err.println("Es gab einen Fehler mit der Transformer- Konfiguration.\n" + e.getMessage());
		} catch (TransformerException e) {
			System.err.println("Es gab einen Fehler mit dem Transformer.\n" + e.getMessage());
			e.printStackTrace();
		}

	}

}
