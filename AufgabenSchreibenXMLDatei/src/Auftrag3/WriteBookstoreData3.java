package Auftrag3;

/* Arbeitsauftrag:  Speichern Sie die Liste der B�cher 
 * 					(buchliste) in die Datei "buchhandlung.xml".
 * 					Dabei gehen Sie wie folgt vor:
 *                  - Erstellen Sie ein DOM-Dokument  
 * 					- sichern Sie es als XML in die Datei "buchhandlung.xml".
 * 
 * 	Hinweis: Die Struktur der Ergebnisdatei soll der Datei 
 *           "Vorgabe_f�r_Ausgabedatei.xml" entsprechen. 			
 *               
 */

import java.io.*;
import java.util.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;

public class WriteBookstoreData3 {

	public static void main(String[] args) {

		List<Buch> buchliste = new ArrayList<>();
		buchliste.add(new Buch("Everyday Italian", "Giada De Laurentiis", 30.0));
		buchliste.add(new Buch("Harry Potter", "J K. Rowling", 29.99));
		buchliste.add(new Buch("XQuery Kick Start", "James McGovern", 49.99));
		buchliste.add(new Buch("Learning XML", "Erik T. Ray", 39.95));

		// Document vorbereiten
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dbuilder;
			dbuilder = factory.newDocumentBuilder();
			Document doc = dbuilder.newDocument();
			
			Element buchhandlung = doc.createElement("buchhandlung");
			doc.appendChild(buchhandlung);
			
			for (Buch buch : buchliste) {
				Element einBuch = doc.createElement("buch");
				
				Element einTitel = doc.createElement("titel");
				einTitel.setTextContent(buch.getTitel());
				einBuch.appendChild(einTitel);
				Element einAutor = doc.createElement("autor");
				einAutor.setTextContent(buch.getAutor());
				einBuch.appendChild(einAutor);
				Element einPreis = doc.createElement("preis");
				einPreis.setTextContent(String.valueOf(buch.getPreis()));
				einBuch.appendChild(einPreis);
				
				buchhandlung.appendChild(einBuch);
			}
			
			// XML Schreiben
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				
				// Ziel f�r das Schreiben setzen
				StreamResult result = new StreamResult(new File("src/Auftrag3/buchhandlung.xml"));
				
				// Datei schreiben
				transformer.transform(source, result);
			
		} catch (ParserConfigurationException e) {
		
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
		
			e.printStackTrace();
		} catch (TransformerException e) {
		
			e.printStackTrace();
		}


		
		
	}

}
