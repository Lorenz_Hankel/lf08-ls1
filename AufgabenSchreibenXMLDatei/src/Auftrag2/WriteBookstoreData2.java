package Auftrag2;

/* Arbeitsauftrag:  Erstellen Sie ein DOM-Dokument gem�� den Vorgaben 
 * 					aus der Datei "Vorgabe_f�r_Ausgabedatei.xml" 
 * 					und sichern Sie es als XML in eine Datei 
 * 					mit dem Filename "buchhandlung.xml".
 * 	
 *               
 */

import java.io.File;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;

public class WriteBookstoreData2 {

	public static void main(String[] args) {

		//Add your code here
try {
			
			// Document vorbereiten
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dbuilder = factory.newDocumentBuilder();
			Document doc = dbuilder.newDocument();
			
			Element buchhandlung = doc.createElement("buchhandlung");
			doc.appendChild(buchhandlung);
			
			Element buch = doc.createElement("buch");

			
			Element titel = doc.createElement("titel");
			titel.appendChild(doc.createTextNode("Java ist auch eine Insel"));
			
			buch.appendChild(titel);
			
			Element autor = doc.createElement("autor");
			autor.appendChild(doc.createTextNode("Christian Ullenboom"));
			
			buch.appendChild(autor);
			
			buchhandlung.appendChild(buch);
			// XML Schreiben
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			
			// Ziel f�r das Schreiben setzen
			StreamResult result = new StreamResult(new File("src/Auftrag2/buchhandlung.xml"));
			
			// Datei schreiben
			transformer.transform(source, result);
			
		} catch (ParserConfigurationException e) {
			System.err.println("Es gab einen Fehler mit einen Parser.\n" + e.getMessage());
		} catch (TransformerConfigurationException e) {
			System.err.println("Es gab einen Fehler mit der Transformer- Konfiguration.\n" + e.getMessage());
		} catch (TransformerException e) {
			 System.err.println("Es gab einen Fehler mit dem Transformer.\n" + e.getMessage());
			e.printStackTrace();
		}
		
		
	}

}
