import java.io.BufferedReader;
import java.io.Console;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Klasse f�r die Main Methode
 * 
 * @author Lorenz Hankel
 * @version 10.08.2021
 *
 */
public class CSVDateiLesenBeispiel1 {

	/**
	 * Main Methode des CSV Lesen Projektes<br>
	 * Liest eine CSV Datei ein zur weiteren Bearbeitung.<br>
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// Variablen deklarieren und initialisieren
		String dateiNameIn = "studentNameCSV.csv";
		try {
			// ich definiere das Buffered Reader Objekt mit einem neuen File Reader Objekt
			// Der Dateiname ist "artikelliste.CSV".
			BufferedReader reader = new BufferedReader(new FileReader(dateiNameIn));
			String zeile = "";
			zeile = reader.readLine();
			while (zeile != null) {
				System.out.println("Datens�tze: " + zeile);
				zeile = reader.readLine();
			}

		} catch (FileNotFoundException e) {
			System.out.println("Keine Datei gefunden mit dem Pfad: " + dateiNameIn + ".");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Dateizugriff verw�hrt!");
			e.printStackTrace();
		}
	}

}
