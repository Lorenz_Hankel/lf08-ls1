import java.io.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/* Arbeitsauftrag:  Lesen Sie den Titel des Buches "Java ist auch eine Insel" aus der Datei  
 *                  "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *                  Ausgabe soll wie folgt aussehen:
 *                     titel:  Java ist auch eine Insel                  
 */

/**
 * Main Klasse des Projektes
 * 
 * @author Lorenz Hankel
 * @version 12.08.2021
 *
 */
public class ReadBookstoreData1 {

	/**
	 * Main Funktion des Projektes, <br>
	 * soll XML Daten einlesen und ausgeben.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("read_buchhandlung.xml");
			NodeList titelList = doc.getElementsByTagName("titel");
			
			for(int i = 0; i < titelList.getLength(); i++) {
				Node titelNode = titelList.item(i);			
				System.out.println(titelNode.getNodeName() + ": " + titelNode.getTextContent());
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
