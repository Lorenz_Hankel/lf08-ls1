
/**
 * @author dariush
 *
 */
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import java.io.FileReader;
import java.io.IOException;

public class CSVHandler {

	// ge�ndert von "studentNameCSV.txt" zu "studentNameCSV.csv".
	private String file = "studentNameCSV.csv"; // muss sich im aktuellen Ordner befinden!
	private String delimiter = ";";
	private String line = "";

	// Constructor 1
	public CSVHandler() {
	}

	// Constructor 2
	public CSVHandler(String delimiter, String file) {
		super();
		this.delimiter = delimiter;
		this.file = file;
	}

	// Begin Methods

	public List<Schueler> getAll() {
    Schueler s = null;
    List<Schueler> students = new ArrayList<Schueler>();
    String name = "";
    int joker = 0;
    int blamiert = 0;
    int fragen = 0;
    
    try {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		line = reader.readLine();
		while (line != null) {
			// name , joker, blamiert, fragen
			line = reader.readLine();
			if (line == null) {
				break;
			}
			
			//String zerlegen
			String infoListe[] = line.split(";");
			
			s = new Schueler(infoListe[0], infoListe[1], joker = Integer.parseInt(infoListe[2]) , blamiert = Integer.parseInt(infoListe[3]), fragen = Integer.parseInt(infoListe[4]));
			students.add(s);
			
			
		}
      
    } catch (IOException e) {
      
      System.out.println("Dateizugriff verweigert.");
      
    }
    
    return students;
  }

	public void printAll(List<Schueler> students) {
		System.out.println("Name\t\tJoker\tblamiert\tFragen");
		for (Schueler s : students) {
			System.out.println(s.getVorname() + " " + s.getName() + "\t" + s.getJoker() + "\t" + s.getBlamiert() + "\t\t" + s.getFragen());
		}
	}
}