/**
  *
  * MEINS
  *
  * @version 10.08.2021
  * @author Lorenz Hankel
  */

public class Schueler {

  // Anfang Attribute
  private String name;
  private String vorname;
  private int joker;
  private int blamiert;
  private int fragen;
  // Ende Attribute

  //Konstruktor
  public Schueler(String vorname, String name, int joker, int blamiert, int fragen){
    this.vorname = vorname;
	this.name = name;
    this.joker = joker;
    this.blamiert = blamiert;
    this.fragen = fragen;
  }
  //Zugriffsmethoden
  // Anfang Methoden
  public String getName() {
    return name;
  }

  public int getJoker() {
    return joker;
  }

  public int getBlamiert() {
    return blamiert;
  }
  

  public String getVorname() {
	return vorname;
}
public int getFragen() {
    return fragen;
  }

  public void blamiert() {
    this.blamiert++;
  }

  public void gefragt() {
    this.fragen++;
  }

  public void nichtDa(){
    this.fragen--;
  }

  public void joker_eingesetzt() {
    this.joker++;
  }
  
  

  public String toString(){
    return this.name + "(Joker: " + this.joker + ", Fragen: " + this.fragen + ")";
  }
  // Ende Methoden
}
